# Intro

Salut, il y a toutes les features demandées dans l'énoncé, j'ai rajouté la possibilité d'effacer le dernier item de la liste. 

## Explications 

- J'ai mis en place du docker pour pouvoir builder le projet rapidement, tout est expliqué dans le README sur comment lancer le projet. 
Pour le swagger je le génére à partir de docstrings. Il faut juste lancer la commande pour générer l'inteface web qui donne accès au swagger. 

- Je n'ai pas eu le temps de faire de partie front dans le temps impartie, en revanche je vous fourni une collection postman que vous pouvez utiliser pour faire les tests sur l'api. Vous avez aussi la page du swagger qui est disponible.

- Je ne savais pas si vous vouliez une api pure ou une application utilisable, donc j'ai fait le choix de partir sur une application fonctionnelle qui permet d'ajouter des nombres dans une liste et de faire les operations au fur et à mesure dessus. On a donc pas une api au sens plus traditionnel du terme, mais plus une application directement utilisable. 
J'ai mis un petit exemple d'utilisation dans le README

- On peut améliorer le code il y a beaucoup de code dupliqué ou du moins semblable entre les routes. Avec un peu plus de temps, on pourrait créer, par exemple un controller plus générique et ainsi diminuer la quantité de code dans le fichier principal.

- J'ai commencé à faire la structure pour une future évolution avec un front. Nous aurions fait ça avec docker. 
