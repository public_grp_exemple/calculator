# Roadmap

## Liste des fonctionnalités que l'on souhaite développer

### Technique 

- Ajouter un front exemple: angular
- Ajouter le service front angular au sein du projet et de l'architecture docker
- Corriger le docker-compose en conséquence 
- Ecriture de tests


### Fonctionnel 

- Ajouter la possibilité d'utiliser cos, sin, tan
- Ajouter la possibilité d'utiliser acos, asin, atan
- Ajouter la possibilité de faire un nombre au carré
- Ajouter la possibilité de faire la racine carrée
- Ajouter la possibilité d'ajout de plusieurs nombres à la suite dans la queue
