import json
from flask import Flask
from flask import request
from jsonschema import validate, ValidationError
from flask import session
from flask import jsonify
from flask_cors import CORS, cross_origin
from flasgger import Swagger


app = Flask(__name__)
swagger = Swagger(app)
CORS(app)
app.secret_key = 'super secret key'
SESSION_TYPE = 'filesystem'
app.config.from_object(__name__)
schema = {
    "type" : "object",
    "properties" : {
        "number" : {"type" : "number"},
    },
    "additionalProperties": False
}

# init route
@app.route('/')
def hello_init():
    """Endpoint to init queue in the app
    ---
    definitions:
      ListNumber:
        type: object
        properties:
          response:
            type: string
            items:
              $ref: '#/definitions/result'
      result:
        type: string
    responses:
      200:
        description: init the list of value
        schema:
          $ref: '#/definitions/ListNumber'
        examples:
          response: 'Hello, welcome to the little app calculator!'
    """
    try:
        app.logger.debug('Start function hello_init')
        response = jsonify()
        session['list_number'] = []
        response = json.dumps({'response':'Hello, welcome to the little app calculator!'})
        return response

    except Exception as error:
        app.logger.error('An error happened %s' % error)
        return error

# basic methods, list the number, add number, delete number and clean queue

@app.route('/list_number')
def see_list():
    """ Endpoint returning the list of numbers
    ---
    definitions:
      ListNumber:
        type: object
        properties:
          response:
            type: array
            items:
              $ref: '#/definitions/Num'
      Num:
        type: integer
    responses:
      200:
        description: List of number
        schema:
          $ref: '#/definitions/ListNumber'
        examples:
          response: [1, 2, 3]
    """
    try:
        app.logger.debug('Start function see_list')
        if "list_number" in session:
            number_list = session["list_number"]
            return json.dumps({'response':number_list})

        else:
            return json.dumps({'response':''})

    except Exception as error:
        app.logger.error('An error happened %s' % error)
        return json.dumps({'ERROR':'An error happened'}), 500


@app.route('/add', methods=['POST'])
def add_number():
    """Endpoint to add an number to the list
    ---
    parameters:
      - name: number
        in: body
        type: number
        required: true
        default: all
    definitions:
      Number:
        type: object
        properties:
          number:
            type: integer
            items:
              $ref: '#/definitions/Num'
      Num:
        type: integer
    responses:
      201:
        description: add number to the list
        schema:
          $ref: '#/definitions/Number'
        examples:
          number: 100
    """
    try:
        app.logger.debug('Start function add_number')
        validate(instance=request.json, schema=schema)
        number = request.json["number"]
        session["list_number"].append(number)
        session.modified = True
        app.logger.debug("list = %s" % session["list_number"])
        return json.dumps({'response':'number has been added'}), 201

    except ValidationError as error:
        app.logger.warning('An error happened %s' % error)
        return json.dumps({'ERROR':'Bad request, check data format'}), 400

    except Exception as error:
        app.logger.error('An error happened %s' % error)
        return json.dumps({'ERROR':'An error happened'}), 500


@app.route('/delete', methods=['GET'])
def delete_number():
    """ Endpoint delete the last item in the queue
    ---
    definitions:
      ListNumber:
        type: object
        properties:
          response:
            type: string
            items:
              $ref: '#/definitions/result'
      result:
        type: string
    responses:
      200:
        description: delete the last item in the queue
        schema:
          $ref: '#/definitions/ListNumber'
        examples:
          response: 'number deleted is 100'
    """
    try:
        app.logger.debug('Start function delete_number')
        if len(session["list_number"]) > 0:
            deleted_number = session["list_number"].pop()
            session.modified = True
            app.logger.debug("list = %s" % session["list_number"])
            return json.dumps({'response':'number deleted is %s' % deleted_number})
        
        else:
            return json.dumps({'response':'Queue empty nothing to delete'})

    except Exception as error:
        app.logger.error('An error happened %s' % error)
        return json.dumps({'ERROR':'An error happened'}), 500


@app.route('/clean', methods=['GET'])
def clean_list():
    """ Endpoint clean the queue
    ---
    definitions:
      ListNumber:
        type: object
        properties:
          response:
            type: string
            items:
              $ref: '#/definitions/result'
      result:
        type: string
    responses:
      200:
        description: execute the cleaning operation of the queue
        schema:
          $ref: '#/definitions/ListNumber'
        examples:
          response: 'list has been cleaned'
    """
    try:
        app.logger.debug('Start function clean_list')
        session["list_number"] = []
        session.modified = True
        app.logger.debug("list = %s" % session["list_number"])
        return json.dumps({'response':'list has been cleaned'})

    except Exception as error:
        app.logger.error('An error happened %s' % error)
        return json.dumps({'ERROR':'An error happened'}), 500


# Addition route
@app.route('/addition', methods=['GET'])
def addition():
    """ Endpoint returning the addition between the last two numbers
    ---
    definitions:
      ListNumber:
        type: object
        properties:
          response:
            type: string
            items:
              $ref: '#/definitions/result'
      result:
        type: string
    responses:
      200:
        description: execute the addition operation
        schema:
          $ref: '#/definitions/ListNumber'
        examples:
          response: 'addition result = 1'
    """
    try:
        app.logger.debug('Start function addition')
        if len(session["list_number"]) < 2:
            raise Exception("Not enough number in queue, please add a minimum of 2 numbers ")
        number = session["list_number"][-2]+session["list_number"][-1]
        session["list_number"] = session["list_number"][:-2]
        session["list_number"].append(number)
        session.modified = True
        app.logger.debug("list = %s" % session["list_number"])
        return json.dumps({'response':'addition result = %s' % number})

    except Exception as error:
        app.logger.error('An error happened %s' % error)
        return json.dumps({'ERROR':'An error happened'}), 500

# substraction route 
@app.route('/substraction', methods=['GET'])
def substraction():
    """ Endpoint returning the substraction between the last two numbers
    ---
    definitions:
      ListNumber:
        type: object
        properties:
          response:
            type: string
            items:
              $ref: '#/definitions/result'
      result:
        type: string
    responses:
      200:
        description: execute the substraction operation
        schema:
          $ref: '#/definitions/ListNumber'
        examples:
          response: 'substraction result = 1'
    """
    try:
        app.logger.debug('Start function substraction')
        if len(session["list_number"]) < 2:
            raise Exception("Not enough number in queue, please add a minimum of 2 numbers ")

        number = session["list_number"][-2]-session["list_number"][-1]
        session["list_number"] = session["list_number"][:-2]
        session["list_number"].append(number)
        session.modified = True
        app.logger.debug("list = %s" % session["list_number"])
        return json.dumps({'response':'substraction result = %s' % number})
        
    except Exception as error:
        app.logger.error('An error happened %s' % error)
        return json.dumps({'ERROR':'An error happened'}), 500

# multiplication route
@app.route('/multiplication', methods=['GET'])
def multiplication():
    """ Endpoint returning the multiplication between the last two numbers
    ---
    definitions:
      ListNumber:
        type: object
        properties:
          response:
            type: string
            items:
              $ref: '#/definitions/result'
      result:
        type: string
    responses:
      200:
        description: execute the multiplication operation
        schema:
          $ref: '#/definitions/ListNumber'
        examples:
          response: 'multiplication result = 1'
    """
    try:
        app.logger.debug('Start function multiplication')
        if len(session["list_number"]) < 2:
            raise Exception("Not enough number in queue, please add a minimum of 2 numbers ")

        number = session["list_number"][-2]*session["list_number"][-1]
        session["list_number"] = session["list_number"][:-2]
        session["list_number"].append(number)
        session.modified = True
        app.logger.debug("list = %s" % session["list_number"])
        return json.dumps({'response':'multiplication result = %s' % number})
        
    except Exception as error:
        app.logger.error('An error happened %s' % error)
        return json.dumps({'ERROR':'An error happened'}), 500

# division route
@app.route('/division', methods=['GET'])
def division():
    """ Endpoint returning the division between the last two numbers
    ---
    definitions:
      ListNumber:
        type: object
        properties:
          response:
            type: string
            items:
              $ref: '#/definitions/result'
      result:
        type: string
    responses:
      200:
        description: execute the division operation
        schema:
          $ref: '#/definitions/ListNumber'
        examples:
          response: 'division result = 1'
    """
    try:
        app.logger.debug('Start function division')
        if len(session["list_number"]) < 2:
            return json.dumps({'ERROR':'Not enough number in queue, please add a minimum of 2 numbers'}), 400

        if session["list_number"][-1] == 0:
            return json.dumps({'ERROR':'Division by 0 impossible'}), 400

        number = session["list_number"][-2]/session["list_number"][-1]
        session["list_number"] = session["list_number"][:-2]
        session["list_number"].append(number)
        session.modified = True
        app.logger.debug("list = %s" % session["list_number"])
        return json.dumps({'response':'division result = %s' % number})
        
    except Exception as error:
        app.logger.error('An error happened %s' % error)
        return json.dumps({'ERROR':'An error happened'}), 500
