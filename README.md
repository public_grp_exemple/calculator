# Calculator Project

## Presentation 

Little app claculator 
This app use python and flask
Python version 3.8

## Launch project

First go to directory calculator_back

To launch the project you can use docker 

```
docker build -t calculator .
docker run -p 5000:5000 -it --name calculator calculator
```

If you don't have docker use the following command to start project, go to directory calculator_api
Command to launch project localy without docker 
```
pipenv shell 
pipenv install
```

if you want to activate the debug mode use the following command 
```
export FLASK_DEBUG=1
flask run
```

## Swagger 

To generate swagger use the following command after launch the project
```
docker exec -it calculator bash
python app.py
```
Then you can go to this url to see swagger
http://localhost:5000/apidocs/#/

## Postman 

There is a post man collection in the directory Postman collection
You can import in your postman to have routes and test it 

## Done

- [x] Show list of value
- [x] add item in list
- [x] delete item in the list
- [x] do addition
- [x] do multiplication
- [x] do substraction
- [x] do division
- [x] clean the list


## Usage exemple

- Call route http://127.0.0.1:5000
- Call route http://127.0.0.1:5000/list_number
- Call route http://127.0.0.1:5000/add with body {"number":10}
- Call route http://127.0.0.1:5000/add with body {"number":5}
- Call route http://127.0.0.1:5000/addition

Congratulation you do an addition 


## Documentation link 

- https://github.com/flasgger/flasgger